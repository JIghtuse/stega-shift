#!/bin/bash

file_clean=$1
file_inj025=$2
file_inj5=$3
file_inj30=$4

count() {
  fname=$1
  delta=$2

  counter=0
  while IFS= read -r line; do
    comp_res=`echo "$line > $delta" | bc`
    #if [[ $line -gt $delta ]]; then
    if [[ $comp_res -gt 0 ]]; then
      counter=$((($counter+1)))
    fi
  done < "$fname"
}

gen_roc() {
#  for delta in $(seq 0 100); do
for ((i = -1; i < 200; i += 1)) do
  delta=$(echo "scale=5; $i/2" | bc)

    count $file_clean $delta;
    clean_str=$(echo "scale=20; $counter / 1000." | bc)
    count $file_inj025 $delta;
    inj025_str=$(echo "scale=20; $counter / 1000." | bc)
    count $file_inj5 $delta;
    inj5_str=$(echo "scale=20; $counter / 1000." | bc)
    count $file_inj30 $delta;
    inj30_str=$(echo "scale=20; $counter / 1000." | bc)

    echo "0$clean_str 0$inj025_str 0$inj5_str 0$inj30_str"
  done
}

gen_roc
