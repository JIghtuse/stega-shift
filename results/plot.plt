#!/usr/bin/gnuplot -p

set term pngcairo enhanced size 800,600
set grid
set key bottom right reverse Right box 1
set output 'shift.png'
plot [0:1] 'plot.data' u 1:2 title '2.5%' w lines, \
           'plot.data' u 1:3 title '5%' w lines, \
           'plot.data' u 1:4 title '30%' w lines, \
           x w lines
