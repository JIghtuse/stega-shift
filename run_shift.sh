#!/bin/bash

INJECT_BIN=$HOME/code/master/steganography/inject/bin/inject
SHIFT_DETECT_BIN=$HOME/code/master/steganography/shift/bin/detect

ARCHIVE_BIN=xz
ARCHIVE_EXT=xz

detect() {
  $SHIFT_DETECT_BIN $inj_file

  file=$(ls incr_$inj_file)
  compress
  file=$(ls decr_$inj_file)
  compress
  file=$(ls reference_$inj_file)
  compress
  incr=$(ls -l incr_$inj_file.$ARCHIVE_EXT | cut -d ' ' -f 5)
  decr=$(ls -l decr_$inj_file.$ARCHIVE_EXT | cut -d ' ' -f 5)
  avg=$(( ($incr + $decr) / 2 ))

  prev=0
  prev_perc=0
  for ((j = 0; j <= 200; j += 1)) do
    ref_perc=`echo "scale=5; $j/2.0" | bc`
    $INJECT_BIN rrnd reference_$inj_file $ref_perc > /dev/null
    file=$(ls inj_*reference_$inj_file)
    compress
    sz=$(ls -l inj_*reference_$inj_file.$ARCHIVE_EXT | cut -d ' ' -f 5)
    if [[ $avg -le $sz ]]; then
      val=$(echo "scale=5; ($prev_perc + $ref_perc) / 2.0" | bc)
      echo $val
#      echo "interpolating between $prev and $sz ($prev_perc% and $ref_perc%)";
      break
    fi
    prev=$sz
    prev_perc=$ref_perc
    rm -r inj_*reference_$inj_file.$ARCHIVE_EXT
    rm -r inj_*reference_$inj_file
  done
  if [[ $ref_perc == "100" ]]; then
    echo "100";
  fi

  rm -r incr_$inj_file decr_$inj_file reference_$inj_file
  rm *$inj_file.$ARCHIVE_EXT
}

compress() {
#  $ARCHIVE_BIN -m5 a $file.rar $file > /dev/null;
  $ARCHIVE_BIN -k $file > /dev/null;
}

run_on_empty() {
  #echo "running on empty files"
  inj_file=$fname_orig
  percentage=0
  detect
  rm -r inj_*$fname_orig
}

run_inject() {
  #echo "running with injection"
  $INJECT_BIN rrnd $fname_orig $percentage > /dev/null

  for inj_file in $(ls inj_*$fname_orig | grep -v reference); do
    detect
  done
  rm -r inj_*$fname_orig
}

run_on_files() {
  for i in $(seq $1 $2); do
    fname_orig=$i.bmp
#    run_on_empty
    percentage=2.5
    run_inject
  done
}

run_on_files $1 $2
