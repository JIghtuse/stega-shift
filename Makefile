RUSTC := rustc
MKDIR := mkdir -p
BIN := bin/detect

all: $(BIN)

$(BIN): src/main.rs src/bmp.rs src/shift.rs
	$(MKDIR) bin
	$(RUSTC) $< -o $@

run: $(BIN)
	./bin/detect ../content/001.bmp_rrand

clean:
	rm -f $(BIN)
