use std::io::File;
use std::vec::Vec;

struct BitmapFileHeader {
    header: Vec<char>,
    fsize: u32,
    res1: u16,
    res2: u16,
    data_offset: uint
}

#[inline]

pub struct BitmapContainer {
    header: Vec<u8>,
    pub size: uint,
    bytes: Vec<u8>,
}

impl Clone for BitmapContainer {
    fn clone(&self) -> BitmapContainer {
        return BitmapContainer {
            header: self.header.clone(),
            size: self.size,
            bytes: self.bytes.clone(),
        }
    }
}

impl BitmapContainer {
    pub fn getbyte(&self, pos: uint) -> u8 {
        return *self.bytes.get(pos);
    }
    pub fn putbyte(&mut self, pos: uint, b: u8) {
        self.bytes.grow_set(pos, &b, b);
    }
}

fn u8_pack_u32(arr: &[u8]) -> u32 {
    return arr[0] as u32
         | arr[1] as u32 << 8
         | arr[2] as u32 << 16
         | arr[3] as u32 << 24;
}
#[inline]
fn u8_pack_u16(arr: &[u8]) -> u16 {
    return arr[0] as u16 | arr[1] as u16 << 8;
}

fn get_nbytes(contents: &Vec<u8>) -> uint {
    let dib_header_sz = u8_pack_u32(contents.slice(14, 18));
    let mut nbytes : u32;
    if dib_header_sz == 12 {
        nbytes = u8_pack_u16(contents.slice(18, 20)) as u32
                * u8_pack_u16(contents.slice(20, 22)) as u32;
    } else {
        nbytes = u8_pack_u32(contents.slice(18, 22)) as u32
                * u8_pack_u32(contents.slice(22, 26)) as u32;
    }
    let ncolors = 3u;
    return ncolors * nbytes as uint;
}

pub fn load_image(fname: &str) -> BitmapContainer {
    let path = Path::new(fname);
    let mut file = match File::open(&path) {
        Ok(f) => f,
        Err(e) => fail!("file opening error: {}", e),
    };

    let contents = file.read_to_end().unwrap();
    let fheader = BitmapFileHeader {
        header: vec!(*contents.get(0) as char, *contents.get(1) as char),
        fsize: u8_pack_u32(contents.slice(2,6)),
        res1: u8_pack_u16(contents.slice(6,8)),
        res2: u8_pack_u16(contents.slice(8, 10)),
        data_offset: u8_pack_u32(contents.slice(10, 14)) as uint,
    };
    // FIXME: we need to check header and bpp here

    let nbytes = get_nbytes(&contents);

    let bytes = contents.slice(fheader.data_offset, fheader.data_offset+nbytes);
    let header = contents.slice(0, fheader.data_offset);

    return BitmapContainer {
        header: Vec::from_slice(header),
        size: nbytes,
        bytes: Vec::from_slice(bytes)
    };
}

pub fn save_image(old_path_str: &str, prefix: &str, container: BitmapContainer) -> bool {
    let old_path = Path::new(old_path_str);
    let old_fname = old_path.filename_str().unwrap();
 
    // 1.bmp => inj_rrand_5.9999_1.bmp
    let mut new_fname = prefix.to_owned();
    new_fname = new_fname.append(old_fname);

    let new_path_str = old_path.dirname_str().unwrap().to_owned();

    let mut new_path = Path::new(new_path_str);
    new_path.push(new_fname);

    let mut file = File::create(&new_path);

    file.write(container.header.as_slice()).unwrap();
    file.write(container.bytes.as_slice()).unwrap();
    return true;

}
