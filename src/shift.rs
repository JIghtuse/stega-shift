use bmp::BitmapContainer;

pub fn shift_bitmap(mut container: BitmapContainer) -> BitmapContainer {
    for i in range(0u, container.size) {
        let byte = container.getbyte(i);
        container.putbyte(i, byte >> 1);
    }
    return container;
}


pub fn incr_bitmap(mut container: BitmapContainer) -> BitmapContainer {
    for i in range(0u, container.size) {
        let byte = container.getbyte(i);
        if byte == 255 {
            continue;
        }
        container.putbyte(i, byte + 1);
    }
    return container;
}

pub fn decr_bitmap(mut container: BitmapContainer) -> BitmapContainer {
    for i in range(0u, container.size) {
        let byte = container.getbyte(i);
        if byte == 0 {
            continue;
        }
        container.putbyte(i, byte - 1);
    }
    return container;
}
