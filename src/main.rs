use bmp::{load_image,save_image};
mod bmp;
mod shift;

fn main() {
    let args = std::os::args();
    if args.len() < 2 {
        fail!("Usage: {:s} <filename.bmp>", args.get(0).as_slice());
    }

    let fname = args.get(1).as_slice();
    let container = bmp::load_image(fname);

    let reference = shift::shift_bitmap(container.clone());
    bmp::save_image(fname, "reference_", reference.clone());

    let incr = shift::incr_bitmap(container.clone());
    let shift_incr = shift::shift_bitmap(incr);
    bmp::save_image(fname, "incr_", shift_incr);
    let decr = shift::decr_bitmap(container);
    let shift_decr = shift::shift_bitmap(decr);
    bmp::save_image(fname, "decr_", shift_decr);
}
